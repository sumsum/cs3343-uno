package Uno;

public class UnoChecker {

	
	public boolean hasSpecial(Player p) {
		// TODO Auto-generated method stub
		
		for(Card c:p.PlayerCards()) {
			
			if(c.checkSpecial()) {
				return true;
			}
			
		}
		
		
		return false;
	}

	public boolean isValidChoice(Player p,int choice, Card current) {
		
		/*
		 * checks if the user selection was a valid choice or not
		 * to be a valid choice: the player must have the card, the card must be either the same color or value as the current card(card in play/previously played card)
		 */
		
		if(choice <= p.PlayerCards().size()) {
			//add for special
			
			if(p.PlayerCards().get(choice).getColor().equals(current.getColor()) || p.PlayerCards().get(choice).getValue()==current.getValue() || p.PlayerCards().get(choice).checkSpecial()) {
				return true;
			}
			
			
		}
		
		return false;
	}
	
	
		
	public boolean hasColor(Player p,Card current) {
		/*
		 * checks if player has card of the same color as the current card that is being played
		 */
		for(Card c:p.PlayerCards()) {
			
			if(c.getColor().equals(current.getColor())) {
				return true;
			}
			
			
		}
		
		return false;
	}
	
	public boolean hasValue(Player p, Card current) {  
		/*
		 * checks if the player has the card of the same value as the current that is being played. 
		 */
		
		for(Card c:p.PlayerCards()) {
			
			if(c.getValue()==current.getValue()) {
				
				return true;
				
			}
		}
		
		return false;
	}
	
	
	public boolean canOverride(Player p, Card current) {
		
		/*
		 * checks if player has a wild card (special card)
		 * special cards can be played when you don't have a card with the same color or the value of the card that is being currently played
		 * if the current card is a special card, then player must have a special card with the same or greater value to play.
		 */
		for(Card c:p.PlayerCards()) {
			
			if(c.checkSpecial()) {
				if(c.getValue() >= current.getValue()) {
					return true;
				}
			}
		}
		
		
		return false;
	}
	
	
	
	
	public Card getStartingCard(Deck deck) {
		
		/*
		 * gets a valid starting card.
		 * starting card cannot be a special card
		 */
		
		Card temp = deck.peek();
		while(temp.checkSpecial()) {
			deck.shuffle();
			temp = deck.peek();
		}
		
		temp = deck.getTopCard();
		return temp;
	}
	
	
	


	public boolean isGameOver(Player[] player) {
		
		if(player[0].hasWon()) {
			System.out.println("**************************************************");
			System.out.println("Player 1 has won");
			System.out.println("**************************************************");
			return true;
		}
		
		else if(player[1].hasWon()) {
			System.out.println("**************************************************");
			System.out.println("Player 2 has won");
			System.out.println("**************************************************");
			return true;
		}
		
		return false;
	}



}
