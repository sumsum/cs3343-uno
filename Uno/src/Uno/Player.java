package Uno;

import java.util.ArrayList;
import java.util.Scanner;


public class Player {

	
	
	private ArrayList<Card> cardsOfPlayer; 
	private String pname; //player pname
	
	public Player(String pname) {
		/*
		 * creates a array list that will store player cards
		 * assigns pname to the player
		 */
		
		//player object are created in uno class
		
		cardsOfPlayer = new ArrayList<Card>();
		this.pname = pname;
		
	}
	
	public int numberOfCards() {
		/*
		 * returns number of cards player has in hand
		 */
		return cardsOfPlayer.size();
	}
	
	public ArrayList<Card> PlayerCards(){
		/*
		 * returns all the cards player has in hand as an ArrayList
		 * This is used mainly to check if player has any valid cards to play.(Check the Uno class)
		 */
		
		return cardsOfPlayer;
	}
	
	
	public void pickCards(Card c) {
		/*
		 * 
		 */
		cardsOfPlayer.add(c);
		
	}
	
	public Card throwCard(int c) {
		/*
		 * player throws a card from hand which is at position 'c'. c is a integer value and is passed as an argument.
		 */
		
		
		return cardsOfPlayer.remove(c);
	}
	
	public void sayUno() {
		/*
		 * player says uno if they only have 1 card in the hand.
		 */
		Scanner s = new Scanner(System.in);
		
		if (cardsOfPlayer.size()==1){
			
			System.out.println("Uno");
			System.out.println("Press Enter...");
			s.nextLine();
		}
	}
	
	
	public void showCards() {
		/*
		 * this is graphical representation of a card
		 * just to make cards look more look like cards
		 * used in showboard() method in Uno class
		 */
		String c = toString() + ": ";
		
				
		for(int j=0;j<cardsOfPlayer.size();j++) {
				
			if(!cardsOfPlayer.get(j).checkSpecial())
				c += cardsOfPlayer.get(j).getColor() + cardsOfPlayer.get(j).getValue() + " ";
			else
				c += cardsOfPlayer.get(j).getValue() + " ";
		}
		
		System.out.println(c);
	}
	
	public void hideCards() {
		
		/*
		 * to hide player cards
		 * used in showboard() method in Uno class
		 */
		
		String c = toString() + " has " + cardsOfPlayer.size() + " card(s) left.";
		
		System.out.println(c);
	}
	
	public boolean hasWon() {
		/*
		 * checks if player has won or not
		 */
		if(cardsOfPlayer.size()==0) {
			return true;
		}
		return false;
	}
	
	public String toString() {
		/*
		 * text representation of player
		 */
		return this.pname;
	}
	
	
	
	
}
