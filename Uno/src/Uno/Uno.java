package Uno;

import java.util.ArrayList;
import java.util.Scanner;

public class Uno {

	private Card current; // the current card or previously played card or starting card
	private Deck deck; // the deck of the game
	private ArrayList<Card> cardpile; //when players throw card, it piles up here. Also, creates a new deck if the current deck is empty
	private int penalty; // when wildcards stack up the penalty stacks up. If a player is unable to counter the current wildcard on play, player is penalised "penalty" number of cards
	private Scanner choice;
	private Player[] player=new Player[2]; //player 1 and 2
	private int pick; // players pick
	private UnoChecker checker=new UnoChecker(); //perform different valid checking
	
	public Uno() {
		/*constructor
		 * constructs the game
		 * prepares the game to play
		 */
		deck = new Deck();
		deck.shuffle();
		penalty = 0;
		current = checker.getStartingCard(deck);
		cardpile = new ArrayList<Card>();
		cardpile.add(current);
		choice = new Scanner(System.in);
		player[0] = new Player("Player 1");
		player[1] = new Player("Player 2");
		distribute();
		
	
	}
	
	
	
	public void game() {
			/* this method simulates turns between the two players. when turn is even, player 1 plays and when turn is odd player 2 plays.
			*/	
		int turn=0;
		while(!checker.isGameOver(player)) {
			if(turn%2==0) {
			playGame(player[0]);}
			else {
			playGame(player[1]);}
			turn++;
		}
		
	}
	
	
	private void distribute() {
		//this method distributes cards to the players
		for(int i=0;i<10;i++) {
			
			if(i%2==0) {
				player[0].pickCards(deck.getTopCard());
			}
			
			else {
				player[1].pickCards(deck.getTopCard());
			}
			
			
		}
	}
	
	
	
	public void playGame(Player p) {
		/*	 this method takes player that is currently playing as an argument.
			 this method contains entire process for the game.
		*/
		clear();
		decorate();
		System.out.println(p+", It is your turn\nThe current card on play is:\n"+current);
		
		decorate();
		showBoard(p);
		decorate();
		
		if(current.checkSpecial()) {
			penalty+=current.getValue();
			Card pick;
			if(!checker.canOverride(p,current)) {
				System.out.println("You dont have a card to override the current special card, so you are penalised");
				for(int i=0;i<penalty;i++) {

					if(deck.isEmpty()) {
						deck = new Deck(cardpile);
					}
					pick = deck.getTopCard();
					p.pickCards(pick);
					System.out.println("You picked: \n"+pick);
					pause();
					
				}
				penalty = 0;
				current = deck.getTopCard();
				System.out.println("The new current card is: \n"+current);
			}
			
			
			}
			
//		}   
		
		
	
		
	
		if(!checker.hasColor(p,current) && !checker.hasValue(p,current) && !checker.hasSpecial(p)) {
				Card pick=null;	
				System.out.println("You dont have a valid card to play, so you have to pick cards.");
				while(!checker.hasColor(p,current) && !checker.hasValue(p,current) && !checker.hasSpecial(p)) {
					
					pause();
					pick = deck.getTopCard();
					p.pickCards(pick);
					System.out.println("You picked:\n"+pick);
					
				}
				
				System.out.println("You recieved a valid card!");
				pause();
				System.out.println("You have the following cards: ");
				p.showCards();
			}
			
	
		
		
		
		System.out.println("Please pick a card:");
		pick = choice.nextInt()-1;
		//System.out.println(pick);
		
		while(!checker.isValidChoice(p,pick,current)) {
			
			System.out.println("Invalid pick. Please pick a valid card.");
			pick = choice.nextInt()-1;
			
		}
		
		Card play = p.throwCard(pick);
		
		p.sayUno();
		current = play;
		cardpile.add(current);
		//reviveDeck();
	}
	
	
	private void pause() {
		/*
		 * creates a pause
		 * asks the user to press enter
		 * purpose is simply to get user engagement
		 */
		System.out.println("Press enter to continue......");
		choice.nextLine();
		
	}
	

	
	private void decorate() {
		/*
		 * draws asterik lines
		 */
		
		
		System.out.println("***********************************************************************************");
	}
	
	


	public void showBoard(Player p) {
	
		if(p.toString().equals("Player 1")) {
			player[0].showCards();
			player[1].hideCards();
			System.out.println("");
		}
		else {
			player[1].showCards();
			player[0].hideCards();
			System.out.println("");
			
		}
		
	}
	
	private void clear() {
		try{
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		}
		catch(Exception ex){}
	}



}
