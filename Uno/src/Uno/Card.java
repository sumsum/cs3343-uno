package Uno;

public class Card {
	
	private String color;
	private int value;
	private int isSpecialValue;
	private boolean isSpecial;

	public Card(int value,String color) {
		
		/*
		 * constructs a non isSpecial card
		 * sets the color and numerical value
		 * sets it to the normal card category
		 */
		
		
		this.color = color;
		this.value = value;
		this.isSpecialValue = 0;
		this.isSpecial = false;
	}
	
	
	public Card(int isSpecialValue) { // constructor for isSpecial cards like +4 and +2	
		
		/*
		 * assigns isSpecial value to the card
		 * sets the card to isSpecial category
		 */
		
		this.color="";
		this.isSpecialValue = isSpecialValue;
		this.value = 0;
		this.isSpecial = true;
	}
	
	public String getColor() {
		/*
		 * returns color of the card
		 */
		
		return this.color;
	}
	
	public int getValue() {
		
		/*
		 * returns numerical value of the card
		 */
		
		if(!this.isSpecial) {
			return this.value;}
		
		else {
			return this.isSpecialValue;
		}
	}
	
	public String toString() {
		
		String c = "";
				
		for(int j=0;j<1;j++) {
			if(!this.checkSpecial())
				c += this.getColor() + this.getValue() + " ";
			else	
				c += this.getValue() + " ";
		}
			
		c +="\n";
		
		return c;
	}
	
	public boolean checkSpecial() {
		
		/*
		 * returns true if card is a isSpecial card
		 * returns false if card is not a isSpecial card
		 */
		
		
		if(isSpecial) {
			return true;
		}
		
		return false;
	}
	
	
	
	
	
}
